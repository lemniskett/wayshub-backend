#!/bin/ash

cp config/config.default.json config/config.json
sed -i "3s/root/$DB_USER/" config/config.json
sed -i "4s/root/$DB_PASSWORD/" config/config.json
sed -i "5s/wayshub/$DB_NAME/" config/config.json
sed -i "6s/127.0.0.1/$DB_HOST/" config/config.json
exec node index.js